package tests;

import io.restassured.http.ContentType;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ApiTests {

    private static final String DEFAULT_API_ENDPOINT = "http://localhost:8080";

    private static String apiEndpoint;

    @BeforeAll
    public static void beforeAll() {
        String apiEndpointPropertyValue = System.getProperty("API_ENDPOINT");

        apiEndpoint = apiEndpointPropertyValue != null ? apiEndpointPropertyValue : DEFAULT_API_ENDPOINT;
    }

    @Test
    @Order(1)
    public void whenCallingToGetCakes_shouldReturn200ResponseWithCakes() {
        when().
                get(apiEndpoint + "/cakes").
        then().
                statusCode(200).
                contentType("application/json").
                body("$", hasSize(5)).

                body("id[0]", notNullValue()).
                body("title[0]", equalTo("Banana cake")).
                body("desc[0]", equalTo("Donkey kongs favourite")).
                body("image[0]", equalTo("http://ukcdn.ar-cdn.com/recipes/xlarge/ff22df7f-dbcd-4a09-81f7-9c1d8395d936.jpg")).

                body("id[1]", notNullValue()).
                body("title[1]", equalTo("Birthday cake")).
                body("desc[1]", equalTo("a yearly treat")).
                body("image[1]", equalTo("http://cornandco.com/wp-content/uploads/2014/05/birthday-cake-popcorn.jpg")).

                body("id[2]", notNullValue()).
                body("title[2]", equalTo("Carrot cake")).
                body("desc[2]", equalTo("Bugs bunnys favourite")).
                body("image[2]", equalTo("http://www.villageinn.com/i/pies/profile/carrotcake_main1.jpg")).

                body("id[3]", notNullValue()).
                body("title[3]", equalTo("Lemon cheesecake")).
                body("desc[3]", equalTo("A cheesecake made of lemon")).
                body("image[3]", equalTo("https://s3-eu-west-1.amazonaws.com/s3.mediafileserver.co.uk/carnation/WebFiles/RecipeImages/lemoncheesecake_lg.jpg")).

                body("id[4]", notNullValue()).
                body("title[4]", equalTo("victoria sponge")).
                body("desc[4]", equalTo("sponge with jam")).
                body("image[4]", equalTo("http://www.bbcgoodfood.com/sites/bbcgoodfood.com/files/recipe_images/recipe-image-legacy-id--1001468_10.jpg"));
    }

    @Test
    @Order(1)
    public void whenCallingToDownloadCakes_shouldReturn200ResponseWithCakes() {
        when().
                get(apiEndpoint + "/cakes/download").
                then().
                statusCode(200).
                contentType(ContentType.BINARY);
    }

    @Test
    public void whenCallingToCreateCake_shouldReturn200ResponseWithCake() {
        given().
                contentType(ContentType.JSON).
                body(buildCakeJsonString("Choclate Gâteau", "Tasty chocolate cake", "http://image.jpg")).
        when().
                post("/cakes").
        then().
                statusCode(201).
                contentType("application/json").
                body("$", hasKey("id")).
                body("id", notNullValue()).
                body("title", equalTo("Choclate Gâteau")).
                body("desc", equalTo("Tasty chocolate cake")).
                body("image", equalTo("http://image.jpg"));
    }

    @Test
    public void givenNoRequestBody_whenCallingToCreateCake_shouldReturn400Response() {
        given().
                contentType("application/json").
        when().
                post("/cakes").
        then().
                statusCode(400);
    }

    @Test
    public void givenRequestForCakeThatExists_whenCallingToCreateCake_shouldReturn409Response() {
        given().
                contentType("application/json").
                body(buildCakeJsonString("Banana cake", "Another banana cake", "https://image.jpg")).
        when().
                post("/cakes").
        then().
                statusCode(409);
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = { "", "  " })
    public void givenRequestContainsInvalidTitle_whenCallingToCreateCake_shouldReturn400Response(String title) {
        given().
                contentType("application/json").
                body(buildCakeJsonString(title, "Another banana cake", "https://image.jpg")).
        when().
                post("/cakes").
        then().
                statusCode(400);
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = { "", "  " })
    public void givenRequestContainsInvalidDesc_whenCallingToCreateCake_shouldReturn400Response(String desc) {
        given().
                contentType("application/json").
                body(buildCakeJsonString("Jaffa Cake", desc, "https://image.jpg")).
                when().
                post("/cakes").
                then().
                statusCode(400);
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = { "", "  " })
    public void givenRequestContainsInvalidImage_whenCallingToCreateCake_shouldReturn400Response(String image) {
        given().
                contentType("application/json").
                body(buildCakeJsonString("Jaffa Cake", "A tiny cake", image)).
                when().
                post("/cakes").
                then().
                statusCode(400);
    }

    private String buildCakeJsonString(String title, String desc, String image) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("title", title);
        jsonObject.put("desc", desc);
        jsonObject.put("image", image);
        return jsonObject.toString();
    }
}
