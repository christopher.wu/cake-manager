# Change Log

## 1. Move code to GitLab.
- I decided to use GitLab as I am familiar with some CI features they provide.

## 2. Fixes to original code base.
Code can be found on branch: [fix_original_codebase](https://gitlab.com/christopher.wu/cake-manager/-/tree/fix_original_codebase)

- Having looked through the code, although I had decided to migrate the application to use Spring Boot, I chose to attempt to fix the /cakes endpoint and time boxed this task to 1 hour.
- The `mvn jetty:run` command would not start the application due to API incompatibility with the jetty-maven-plugin, this was fixed by using an earlier release version of the plugin.
- The /cakes endpoint fixed by adding the servlet mappings to the web.xml
- With the endpoint working it also confirmed my analysis that the cake data imported would exclude some as duplicates as there is a unique constraint on the cake title.

## 3. Moved application to use Spring Boot.
- Created a basic ReST service using Spring Boot and Spring JPA.
- For package naming convention, as the service is very small and focuses on a single Cake Model, I decide to use package by feature instead of individual packages for controller, repository, config, etc.
- Tested the ReST endpoint layer using Spring Test MockMvc, this would test the interaction to the Controller as HTTP requests, testing the Controller through the method signatures would not catch errors such as misspelt endpoints or incorrect HTTP methods used.

## 4. Added a data loader to retrieve data from json file and save it to the database on application startup.

## 5. Added basic index page to display and add cakes.
- Started off using Spring Thymeleaf to create the web UI with full server side rendering of pages but then opted to create the page using jQuery AJAX calls to the ReST API to populate the page with the list of cakes.
- Used Bootstrap to quickly put the page together.
- Added functionality to create cakes from the web UI using ReST endpoint to create cakes, successful create will add the new cake to the list on the page.

## 6. Dockerised application and push image to GCP Container Registry.
- Chose to push to GCP Container Registry as I hope to also deploy the application to my kubernetes cluster.

## 7. Added API tests using Rest Assured and new endpoint to download cakes as JSON file.
- Added tests as separate maven project but inside the same project repository in a sub directory .tests/api-tests
- Tests designed to run against a known test dataset, to simplify this I used the dataset that is loaded into the in-memory database on start up.
- A fresh instance of the application needs to be started when running the API tests.
- One of requirements is to be able to download cakes as JSON. Also, added endpoint /cakes/download to allow the cakes to be downloaded through the browser.

## 8. Added deployment of cake-manager docker image to GCP Cloud Run
- I had initially decided to use my personal GKE cluster however it uses preemptible nodes, so the nodes external IP addresses are ephemeral and opted to use Cloud Run instead.
- GitLab CI used to deploy the cake-manager docker image to GCP Cloud Run using gcloud.

## 9. Attempted to add API tests to run against staging deployment of the application.
- Added staging and prod deployments, idea was to have the API tests to run against the staging deployment. Pipeline job [850415450](https://gitlab.com/christopher.wu/cake-manager/-/jobs/850415450)
- Disabled API tests, the tests get connection refused errors when trying to make http requests to staging using public gitlab runners.

## 10. Added OAuth2
Code can be found on branch: [oauth2](https://gitlab.com/christopher.wu/cake-manager/-/tree/oauth2)
Deployed version can be found at: https://cake-manager-oauth2-3im3sd5cga-nw.a.run.app

I have not merged this code into the master branch as;
- I was not confident the frontend page is a complete working solution
- CakeApiEndpointTest failed as requests started to return 401, ideal solution to this would be to either disable this or allow some fake access token to be used.

I used Auth0 to quickly integrate authentication into the cake manager application. Auth0 also provides a Sign Up page to register an account, and a Sign In page to authenticate the user.

### Securing the ReST endpoints
I chose to only secure `POST /cakes` (Create a new cake) endpoint.

#### ReST backend
- Setup an API Audience `https://cake-manager-api` on Auth0 to secure ReST endpoints.
- Added spring-security-oauth2 and secured the endpoint as a Resource Server using the API Audience and the public jwks.
- Configured spring security to only authenticate `POST /cake` requests.

#### Web frontend
Note: the javascript code `./static/js/app.js` comes from the Auth0 quickstart guide.

- Created Single Page Application on Auth0 to allow frontend user authentication.
- Used Auth0 quickstart guide to add login and logout functionality.
- Updated add cake feature to get an access token for the resource API `https://cake-manager-api` which is then used as a Bearer token to authenticate against the ReST backend.
- Added logic to gray out the Add and Login buttons when the user is not logged in.
