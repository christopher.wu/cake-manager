# Cake Manager

Latest deployment of the application can be found at: https://cake-manager-3im3sd5cga-nw.a.run.app

Change Log can be found at: https://gitlab.com/christopher.wu/cake-manager/-/blob/master/CHANGELOG.md

## Starting the service locally

To run a server locally execute the following command:
```
mvn spring-boot:run
```

The service web UI is accessible at:
```
http://localhost:8080
```

## Running the Rest Assured API Tests against local cake-manager service
notes:
- API Tests are designed to run against a fixed known test dataset, this dataset is the one fetched from https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json.
- A fresh instance of the cake-manager loads the fixed dataset into the in-memory database.
- Running these tests more than once against a fresh instance of the cake-manager service will fail as the data will have changed after the first run.

```
cd .tests/api-tests
mvn clean test
```

## ReST API

### Retrieve list of cakes
**Endpoint:** GET /cakes

#### Response
| HTTP Code | Description |
| --------- | ----------- |
|200 | Successfully returns cakes as JSON Array of Cakes |

### Create a new cake
**Endpoint:** POST /cakes

#### Request Body
```json
{
    "title": "Chocolate Cake",
    "desc": "A cake made of chocolate",
    "image": "https://cdn.sallysbakingaddiction.com/wp-content/uploads/2013/04/triple-chocolate-cake-4.jpg"
}
```
| Field | Type | Required | Description | Validation |
| ----- | ---- | -------- | ----------- | ---------- |
| title | string | yes | Title of the cake | Cannot be blank |
| desc | string | yes | Description of the cake | Cannot be blank |
| image | string | yes | URL to an image of cake | Cannot be blank |

#### Response
| HTTP Code | Response |
| --------- | -------- |
| 201 | Cake successfully created |
| 400 | Request body has invalid values |
| 409 | Cake with title already exists |

# Summary
I was unable to get the Rest Assured API Tests to run as part of the CI pipeline, the GitLab runner was getting connection refused when attempting to make HTTP requests to the staging version of the application. More investigation will be required to analysis what the issue is.

I time boxed the assignment and left oauth2 as the last task to do. I started to work on an oauth2 version of the application, however it introduced some issues around testing, and I was not too confident on my implementation in the frontend.
- A working demo of the oauth2 version can be found at: https://cake-manager-oauth2-3im3sd5cga-nw.a.run.app
- The code has been committed to branch: [oauth2](https://gitlab.com/christopher.wu/cake-manager/-/tree/oauth2).

## Technologies Used
- Spring
- String Boot
- Spring Data JPA
- Rest Assured
- Bootstrap
- jQuery
- Docker
- GitLab CI
- GCP Cloud Run
- Spring Security oauth2 (branch [oauth2](https://gitlab.com/christopher.wu/cake-manager/-/tree/oauth2) only)
- Auth0 (branch [oauth2](https://gitlab.com/christopher.wu/cake-manager/-/tree/oauth2) only)