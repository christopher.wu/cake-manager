package com.waracle.cakemgr.cake;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.UUID;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CakeController.class)
public class CakeApiEndpointTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CakeRepository cakeRepository;

    @Test
    public void whenCallingGetCakesEndpoint_ShouldReturnCakes() throws Exception {

        when(cakeRepository.findAll()).thenReturn(Arrays.asList(
                new Cake(UUID.fromString("e9ebc135-f164-49f7-85c2-04c01e351633"), "Red Cake", "Red coloured cake", "http://redcake.jpg"),
                new Cake(UUID.fromString("bece9cb8-14b8-4ee9-9590-d6253302b284"), "Green Cake", "Green coloured cake", "http://greencake.jpg")
        ));

        this.mockMvc.perform(get("/cakes")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is("e9ebc135-f164-49f7-85c2-04c01e351633")))
                .andExpect(jsonPath("$[0].title", is("Red Cake")))
                .andExpect(jsonPath("$[0].desc", is("Red coloured cake")))
                .andExpect(jsonPath("$[0].image", is("http://redcake.jpg")))
                .andExpect(jsonPath("$[1].id", is("bece9cb8-14b8-4ee9-9590-d6253302b284")))
                .andExpect(jsonPath("$[1].title", is("Green Cake")))
                .andExpect(jsonPath("$[1].desc", is("Green coloured cake")))
                .andExpect(jsonPath("$[1].image", is("http://greencake.jpg")));
    }

    @Test
    public void whenCallingCreateEndpoint_shouldReturnCreatedCake() throws Exception {

        when(cakeRepository.save(any(Cake.class)))
                .thenAnswer(invocation -> {
                    Cake cakeIn = (Cake) invocation.getArguments()[0];
                    return new Cake(
                            UUID.fromString("e9ebc135-f164-49f7-85c2-04c01e351633"),
                            cakeIn.getTitle(),
                            cakeIn.getDesc(),
                            cakeIn.getImage()
                    );
                });

        this.mockMvc.perform(
                post("/cakes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"title\": \"cake\", \"desc\": \"a cake\", \"image\": \"http://image.jpg\" }"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is("e9ebc135-f164-49f7-85c2-04c01e351633")))
                .andExpect(jsonPath("$.title", is("cake")))
                .andExpect(jsonPath("$.desc", is("a cake")))
                .andExpect(jsonPath("$.image", is("http://image.jpg")));
    }

    @Test
    public void givenCakeExists_whenCallingCreateEndpoint_shouldReturn409Conflict() throws Exception {

        when(cakeRepository.save(any(Cake.class))).thenThrow(new DataIntegrityViolationException("Constraint Violation"));

        this.mockMvc.perform(
                post("/cakes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"title\": \"cake\", \"desc\": \"a cake\", \"image\": \"http://image.jpg\" }"))
                .andDo(print())
                .andExpect(status().isConflict());
    }

    @Test
    public void givenNoRequestBody_whenCallingCreateEndpoint_shouldReturnBadRequest() throws Exception {

        this.mockMvc.perform(
                post("/cakes")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void givenInvalidRequestBody_whenCallingCreateEndpoint_shouldReturnBadRequest() throws Exception {

        this.mockMvc.perform(
                post("/cakes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"title\": \"\", \"desc\": \"\", \"image\": null }"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenCallingDownloadCakesAsJsonEndpoint_shouldReturn() throws Exception {

        when(cakeRepository.findAll()).thenReturn(Arrays.asList(
                new Cake(UUID.fromString("e9ebc135-f164-49f7-85c2-04c01e351633"), "Red Cake", "Red coloured cake", "http://redcake.jpg"),
                new Cake(UUID.fromString("bece9cb8-14b8-4ee9-9590-d6253302b284"), "Green Cake", "Green coloured cake", "http://greencake.jpg")
        ));

        this.mockMvc.perform(get("/cakes/download")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_OCTET_STREAM))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is("e9ebc135-f164-49f7-85c2-04c01e351633")))
                .andExpect(jsonPath("$[0].title", is("Red Cake")))
                .andExpect(jsonPath("$[0].desc", is("Red coloured cake")))
                .andExpect(jsonPath("$[0].image", is("http://redcake.jpg")))
                .andExpect(jsonPath("$[1].id", is("bece9cb8-14b8-4ee9-9590-d6253302b284")))
                .andExpect(jsonPath("$[1].title", is("Green Cake")))
                .andExpect(jsonPath("$[1].desc", is("Green coloured cake")))
                .andExpect(jsonPath("$[1].image", is("http://greencake.jpg")));
    }
}