package com.waracle.cakemgr.cake;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CakeDataLoaderTest {

    @InjectMocks
    private CakeDataLoader cakeDataLoader;

    @Spy
    private ObjectMapper objectMapper = new ObjectMapper();

    @Spy
    private ResourceLoader resourceLoader = new DefaultResourceLoader();

    @Mock
    private CakeRepository cakeRepository;

    @Test
    public void givenValidJsonFileLocation_shouldSaveToRepository() throws Exception {
        ReflectionTestUtils.setField(cakeDataLoader, "url", "classpath:testcakes.json");

        cakeDataLoader.loadData();

        verify(cakeRepository).saveAll(any(List.class));
        verifyNoMoreInteractions(cakeRepository);
    }

    @Test
    public void givenInvalidJsonFileLocation_shouldNotSaveToRepository() {
        ReflectionTestUtils.setField(cakeDataLoader, "url", "malformed_url");

        cakeDataLoader.loadData();

        verifyNoInteractions(cakeRepository);
    }

    @Test
    public void givenNullJsonFileLocation_shouldNotSaveToRepository() {
        ReflectionTestUtils.setField(cakeDataLoader, "url", null);

        cakeDataLoader.loadData();

        verifyNoInteractions(cakeRepository);
    }
}