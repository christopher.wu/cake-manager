$(document).ready(function () {

    $("#add-cake-form").submit(function (event) {
        event.preventDefault();
        add_cake();
    });

    fetch_cakes();
});

async function add_cake() {

    var cake = {}
    cake["title"] = $("#form-title").val();
    cake["desc"] = $("#form-description").val();
    cake["image"] = $("#form-imageURL").val();

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/cakes",
        data: JSON.stringify(cake),
        dataType: "json",
        cache: false,
        timeout: 60000,
        success: function (data) {
            addCakeToList(data);
            $("#add-cake-form")[0].reset();
            $("#model-error-msg").addClass("invisible");
            $("#exampleModal").modal("hide");
        },
        error: function (e) {
            switch(e.status) {
                case 400:
                    $("#model-error-msg span").text("All fields are required.");
                    $("#model-error-msg").removeClass("invisible");
                    break;
                case 409:
                    $("#model-error-msg span").text("Cake with that title already exists.");
                    $("#model-error-msg").removeClass("invisible");
                    break;
                default:
                    $("#model-error-msg span").text("Error occurred while trying to add cake.");
                    $("#model-error-msg").removeClass("invisible");
            }
        }
    });

}

function fetch_cakes() {

    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/cakes",
        cache: false,
        timeout: 60000,
        success: function (data) {
            data.forEach(function(cake) {
                addCakeToList(cake);
            });
        },
        error: function (e) {
            console.log("ERROR : ", e);
        }
    });

}

function addCakeToList(cake) {

    var test = `
        <div id="cake|${cake.id}" class="media text-muted pt-3">
            <img class="mr-3" src="${cake.image}" alt="" width="64" height="64" onerror="this.onerror=null;this.src='https://via.placeholder.com/64';">
            <p class="media-body pb-3 mb-0 lh-125">
                <strong class="d-block text-gray-dark">${cake.title}</strong>
                ${cake.desc}
            </p>
        </div>
    `;

    var cakeList = $("#cake-list").prepend(test);

}