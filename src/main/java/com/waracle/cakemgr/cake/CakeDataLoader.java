package com.waracle.cakemgr.cake;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

@Component
@Slf4j
public class CakeDataLoader {

    @Value("${cake-data-loader.url}")
    private String url;

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CakeRepository cakeRepository;

    @PostConstruct
    public void loadData() {
        log.info("Loading cake data from [{}]", url);

        try {
            Resource resource = resourceLoader.getResource(url);

            List<Cake> cakes = objectMapper.readValue(resource.getURL(), new TypeReference<List<Cake>>() { });
            log.info("Fetched {} cakes from data source", cakes.size());

            List<Cake> uniqueCakes = cakes.stream()
                    .collect(collectingAndThen(toCollection(() -> new TreeSet<>(Comparator.comparing(Cake::getTitle))), ArrayList::new));

            cakeRepository.saveAll(uniqueCakes);

            log.info("Saved {} cakes to database, {} duplicates filtered out", uniqueCakes.size(), cakes.size() - uniqueCakes.size());

        } catch (Exception e) {
            log.warn("Failed to load cake data from [{}], no cake data has been loaded into database.", url, e);
        }
    }
}
