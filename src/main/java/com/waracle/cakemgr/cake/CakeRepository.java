package com.waracle.cakemgr.cake;

import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface CakeRepository extends CrudRepository<Cake, UUID> {
}
