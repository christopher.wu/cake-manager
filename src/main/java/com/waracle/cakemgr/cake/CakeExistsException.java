package com.waracle.cakemgr.cake;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class CakeExistsException extends RuntimeException {

    public CakeExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
