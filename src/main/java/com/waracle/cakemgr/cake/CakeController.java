package com.waracle.cakemgr.cake;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping("cakes")
public class CakeController {

    @Autowired
    private CakeRepository cakeRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @GetMapping(value = "/download", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public byte[] downloadAsJson(HttpServletResponse response) throws JsonProcessingException {
        Iterable<Cake> cakes = cakeRepository.findAll();
        byte[] bytes = objectMapper.writeValueAsBytes(cakes);
        response.setContentLength(bytes.length);
        response.addHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=cakes.json");
        return bytes;
    }

    @GetMapping
    public Iterable<Cake> list() {
        return cakeRepository.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cake create(@Valid @RequestBody Cake cake) {
        try {
            return cakeRepository.save(cake);
        } catch (DataIntegrityViolationException e) {
            throw new CakeExistsException("Cake with title[" + cake.getTitle() + "] already exists", e);
        }
    }

}