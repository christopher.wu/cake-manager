FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD /target/cake-manager-1.0-SNAPSHOT.jar app.jar

EXPOSE 8080

ENTRYPOINT java -Xmx384m -Xms384m -XX:+UseG1GC -Djava.security.egd=file:/dev/./urandom -jar /app.jar
